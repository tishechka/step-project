
const tabTitles = document.querySelectorAll('.serv-tabs-title');
const tabContent = document.querySelectorAll('.serv-tabs-content li');


const activeTabIndex = sessionStorage.getItem('activeTabIndex');


tabContent.forEach(content => content.style.display = 'none');


if (activeTabIndex === null) {
    tabTitles[0].classList.add('active');
    tabContent[0].style.display = 'block';
} else {

    tabTitles[activeTabIndex].classList.add('active');
    tabContent[activeTabIndex].style.display = 'block';
}

tabTitles.forEach((title, index) => {
    title.addEventListener('click', () => {

        tabTitles.forEach(t => t.classList.remove('active'));

        title.classList.add('active');

        tabContent.forEach(content => content.style.display = 'none');

        tabContent[index].style.display = 'block';


        sessionStorage.setItem('activeTabIndex', index);
    });
});


document.addEventListener("DOMContentLoaded", function () {
    const tabs = document.querySelectorAll(".work-tabs-title");
    const images = document.querySelectorAll(".work-grid div");

    tabs.forEach((tab) => {
        tab.addEventListener("click", () => {

            tabs.forEach((t) => t.classList.remove("work-active"));


            tab.classList.add("work-active");


            const category = tab.getAttribute("data-target");


            images.forEach((image) => {
                const imageCategory = image.getAttribute("data-category");
                if (category === "0" || imageCategory === category) {
                    image.style.display = "block";
                } else {
                    image.style.display = "none";
                }
            });
        });
    });


    const allTab = document.querySelector(".work-tab-1");
    allTab.addEventListener("click", () => {

        images.forEach((image) => {
            image.style.display = "block";
        });
    });
});


document.addEventListener("DOMContentLoaded", function () {
    const loadMoreButton = document.getElementById("load-more-button");
    const hiddenImagesContainer = document.querySelector(".hidden-images");
    const workGrid = document.querySelector(".work-grid");

    loadMoreButton.addEventListener("click", function () {

        hiddenImagesContainer.style.display = "grid";


        loadMoreButton.style.display = "none";


        const hiddenImages = hiddenImagesContainer.querySelectorAll("div");
        hiddenImages.forEach((image) => {
            workGrid.appendChild(image);
        });
    });
});


const wrapper = document.querySelector(".carousel-wrapper");
const carousel = document.querySelector(".carousel-list");
const slideShowContainers = document.querySelectorAll(".feedback-slideshow");
const firstCardWidth = carousel.querySelector(".carousel-card").offsetWidth;
const arrowBtns = document.querySelectorAll(".carousel-wrapper i");
const carouselChildrens = [...carousel.children];

let isDragging = false, isAutoPlay = true, startX, startScrollLeft, timeoutId;


let cardPerView = Math.round(carousel.offsetWidth / firstCardWidth);


carouselChildrens.slice(-cardPerView).reverse().forEach(card => {
    carousel.insertAdjacentHTML("afterbegin", card.outerHTML);
});


carouselChildrens.slice(0, cardPerView).forEach(card => {
    carousel.insertAdjacentHTML("beforeend", card.outerHTML);
});


carousel.classList.add("no-transition");
carousel.scrollLeft = carousel.offsetWidth;
carousel.classList.remove("no-transition");


arrowBtns.forEach(btn => {
    btn.addEventListener("click", () => {
        carousel.scrollLeft += btn.id == "left-button" ? -firstCardWidth : firstCardWidth;
    });
});


function updateSlideShow(index) {
    slideShowContainers.forEach((container, i) => {
        if (i === index % slideShowContainers.length) {
            container.style.display = "block";
        } else {
            container.style.display = "none";
        }
    });
}

const dragStart = (e) => {
    isDragging = true;
    carousel.classList.add("dragging");

    startX = e.pageX;
    startScrollLeft = carousel.scrollLeft;
}

const dragging = (e) => {
    if (!isDragging) return;

    carousel.scrollLeft = startScrollLeft - (e.pageX - startX);
}

const dragStop = () => {
    isDragging = false;
    carousel.classList.remove("dragging");
}

const infiniteScroll = () => {

    if (carousel.scrollLeft === 0) {
        carousel.classList.add("no-transition");
        carousel.scrollLeft = carousel.scrollWidth - (2 * carousel.offsetWidth);
        carousel.classList.remove("no-transition");
    }
    else if (Math.ceil(carousel.scrollLeft) === carousel.scrollWidth - carousel.offsetWidth) {
        carousel.classList.add("no-transition");
        carousel.scrollLeft = carousel.offsetWidth;
        carousel.classList.remove("no-transition");
    }

}


carousel.addEventListener("mousedown", dragStart);
carousel.addEventListener("mousemove", dragging);
document.addEventListener("mouseup", dragStop);
carousel.addEventListener("scroll", infiniteScroll);
wrapper.addEventListener("mouseenter", () => clearTimeout(timeoutId));


carousel.addEventListener("scroll", () => {
    const index = Math.floor((carousel.scrollLeft + carousel.offsetWidth / 2) / firstCardWidth);
    updateSlideShow(index);
    infiniteScroll();
});

